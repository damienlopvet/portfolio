"use client";
import React, { useEffect, useState } from "react";
import PhotoCard from "./PhotoCard";
import AboutMe from "./AboutMe";
import Nav from "./Nav";
import Resume from "./Resume";
import Contact from "./Contact";
import Works from "./Works";
import Ignition from "./Ignition";
import Bubbles from "./Bubbles"

const timeout = () => {
	return new Promise((resolve) => setTimeout(resolve, 1000));
};

export default function Home() {
	const [newComponent, setNewComponent] = useState("AboutMe");
	const [previousComponent, setPreviousComponent] = useState(null);
  const [activeComponentForMobile, setActiveComponentForMobile] = useState("AboutMe");

	useEffect(() => {
		async function handleComponentSwitch() {
			if (previousComponent != null) {
				let oldComponent = document.getElementById(previousComponent);
				if (oldComponent && oldComponent.style) {
					oldComponent.style.animation = "slide-out ease-in-out var(--slideTime) forwards";
					oldComponent.style.pointerEvents = "none";
				}
			}
		}
		handleComponentSwitch();
	}, [newComponent]);

	return (
		<main
			id='home'
			className='flex relative lgm:h-screen lgm:overflow-hidden items-center justify-center w-full px-4 pt-24  xs:px-10 pb-4 xl:p-24 xl:pb-4 bg-gradient-to-t to-60% from-ternary to-secondary transition-all duration-300'>
			<Bubbles />

			<div className='relative w-full max-w-[1300px] h-5/6 flex-content lgm:block lgm:z-10'>
				<Ignition />
				<Nav
					activeComponent={activeComponentForMobile}
					setActiveComponent={setActiveComponentForMobile}
					newComponent={newComponent}
					setNewComponent={setNewComponent}
					setPreviousComponent={setPreviousComponent}
				/>
				<PhotoCard
					activeComponent={activeComponentForMobile}
					setActiveComponent={setActiveComponentForMobile}
					newComponent={newComponent}
					setNewComponent={setNewComponent}
					setPreviousComponent={setPreviousComponent}
				/>

				{newComponent != "" && (
					<>
						{" "}
						<div className='hidden lgm:block'>
							{newComponent === "AboutMe" ? (
								<AboutMe />
							) : newComponent === "Resume" ? (
								<Resume />
							) : newComponent === "Works" ? (
								<Works />
							) : newComponent === "Contact" ? (
								<Contact />
							) : (
								<AboutMe />
							)}
						</div>
						<div className='hidden lgm:block'>
							{previousComponent === "AboutMe" ? (
								<AboutMe />
							) : previousComponent === "Resume" ? (
								<Resume />
							) : previousComponent === "Works" ? (
								<Works />
							) : previousComponent === "Contact" ? (
								<Contact />
							) : (
								<AboutMe />
							)}
						</div>
					</>
				)}
				<div className='lgm:hidden flex-content '>
					<AboutMe />
					<Works />
					<Resume />
					<Contact />
				</div>

				{/* <Works /> */}
				{/* <Contact /> */}
				{/* <AboutMe /> */}
				{/* <Resume /> */}
			</div>
		</main>
	);
}
