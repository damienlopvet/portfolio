"use client";
import React from "react";
import Image from "next/image";
import Link from "next/link";
import { FaGithub, FaLinkedinIn } from "react-icons/fa";

const PhotoCard = ({ activeComponent, setActiveComponent, newComponent, setPreviousComponent, setNewComponent }) => {
	const text = "FullStack Developer";
	function handleChange(arg) {
		if (newComponent === arg) return;
		setPreviousComponent(newComponent);
		setNewComponent(arg);
	}
	function handleChangeTablet(arg) {
		if (newComponent != "") {
			setNewComponent("");
			setPreviousComponent("");
		}
		setActiveComponent(arg);
	}

	return (
		<section
			id='photo_card'
			className='mt-10 lgm:mt-0 text-slate-800 lgm:h-full max-h-[780px] w-full lgm:w-[480px] max-w-[620px] flex flex-col justify-between bg-white overflow-hidden rounded shadow-[-15px_-15px_0px_0px_#242F59_,_2px_3px_15px_#00000038] relative z-10'>
			<Image src={"/profilePhoto.jpeg"} alt='Photo de profil' width={480} height={380} className='w-auto lgm:h-[480px] ' />
			<div className='relative h-full max-lgm:min-h-[300px]'>
				<div className='absolute -top-8 xs:-top-12 sm:max-lgm:-top-16 left-0 origin-top-left rotate-[10deg] h-52 w-7/12 bg-white z-10'></div>
				<div className='absolute -top-8 xs:-top-12 sm:max-lgm:-top-16 right-0 origin-top-right rotate-[-10deg] h-52 w-7/12 bg-white z-10'></div>
				<div className='z-20 relative text-center max-lgm:min-h-[300px] h-full font-light flex justify-between flex-col gap-6'>
					<h1 className='text-2xl lgm:text-4xl font-light mt-6'> Damien Lopvet</h1>
					<p className='animate-text text-primary text-xl '>
						{text.match(/./gu).map((char, index) => (
							<span key={`${char}-${index}`} style={{ animationDelay: `${index * 0.05}s` }}>
								{char === " " ? "\u00A0" : char}
							</span>
						))}
					</p>
					<div className='flex gap-6 justify-center flex-grow'>
						<Link href='https://github.com/DamienLopvet' aria-label="link to my github" target='_blank'>
							
							<FaGithub className='size-5' />
						</Link>
						<Link href='https://www.linkedin.com/in/damien-lopvet/' aria-label="link to my linkedin" target='_blank'>
							
							<FaLinkedinIn className='size-5' />
						</Link>
					</div>
					<div className='relative flex flex-col xs:flex-row justify-around items-center xs:h-16'>
						{/* Dividers */}
						<p className='absolute top-0 bg-gradient-to-r from-white to-white via-slate-400 m-0 p-0 h-[2px] leading-none w-full'></p>
						<p className='hidden xs:block xs:absolute top-0 left-1/2 bg-gradient-to-b from-slate-400 to-white m-0 p-0 h-full leading-none w-[1px]'></p>
						<p className='xs:hidden absolute top-16 bg-gradient-to-r from-white to-white via-slate-400 m-0 p-0 h-[2px] leading-none w-full'></p>
						<button type='button' onClick={() => handleChange("Contact")} className='hidden lgm:block w-1/2 p-5 h-full' target='_blank'>
							CONTACT ME
						</button>
						<Link href='/#Contact' type='button' onClick={() => handleChangeTablet("Contact")} className='block lgm:hidden xs:w-1/2 p-5 h-full'>
							CONTACT ME
						</Link>
						<p className='absolute top-0 bg-gradient-to-r from-white to-white via-slate-400 m-0 p-0 h-[2px] leading-none w-full'></p>
						<Link href='DamienLopvetCV.pdf' className='xs:w-1/2 p-5 h-full' target='_blank'>
							DOWNLOAD CV
						</Link>
					</div>
				</div>
			</div>
		</section>
	);
};

export default PhotoCard;
