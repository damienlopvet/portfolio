import React from "react";
import Image from "next/image";
import Link from "next/link";
import { FaWhatsapp, FaPhone, FaGoogle } from "react-icons/fa";
import { MdEmail } from "react-icons/md";
const Contact = () => {
	return (
		<article id='Contact' className='lgm:animated-card componentSize flex flex-col bg-white overflow-y-scroll rounded-tr rounded-br'>
			{/* Resume */}
			<section>
				{/* Title */}
				<div className='relative'>
					<h2 className='font-semibold text-2xl p-6'>
						<span className='text-primary'>Contact&nbsp;</span>ME
					</h2>
					{/* Dividers */}
					<p className='absolute bottom-0 bg-gradient-to-r from-white to-white via-slate-400 m-0 p-0 h-[1px] leading-none w-11/12'></p>
				</div>
				{/* Content */}
				<div className='py-6 flex flex-col justify-center items-center w-full gap-10 text-light'>
					<div>
						<p className='text-center w-full'>Contact directly on whatsapp</p>
						<Link href='https://wa.me/+41782519580?text=Hello%20Damien,%0a' className=' btn flex items-center justify-center flex-row gap-3 w-full'>
							<FaWhatsapp />
							whatsapp
						</Link>
					</div>
					<div>
						<p className='text-center w-full'>Or either send me an Email </p>
						<Link href='mailto:mail@lopvet-damien.com?subject=Looking for a new challenge%3F&body=Hi Damien,%0a%0a' className=' btn flex items-center justify-center flex-row gap-3 w-full'>
							<MdEmail />
							email
						</Link>
					</div>
					<div>
						<p className='text-center w-full'>Same old, same old..</p>
						<Link href='tel:+41782519580' className=' btn flex items-center justify-center flex-row gap-3 w-full'>
							<FaPhone />
							Phone
						</Link>
					</div>
					<div>
						<p className='text-center w-full'>Need help ?</p>
						<Link href='https://googlethatforyou.com/?q=damien%20lopvet' target='_blank' className=' btn flex items-center justify-center flex-row gap-3 w-full'>
							<FaGoogle />
							Search
						</Link>
					</div>
				</div>
			</section>
		</article>
	);
};

export default Contact;
