import { Poppins } from "next/font/google";
import "./globals.css";
import Logo from "./Logo";
const poppins = Poppins({
	weight: ["100", "200", "300", "400", "500", "600", "700"],
	subsets: ["latin"],
});

export const metadata = {
	title: "Porfolio Damien Lopvet",
	description: "Portfolio de Damien Lopvet, développeur web et mobile, Genève, ...",
};

export default function RootLayout({ children }) {
	return (
		<html lang='fr'>
			<body className={`flex flex-col justify-start ${poppins.className}`}>
				<div className='h-20 w-full z-30 top-0 -mt-20 sticky lgm:hidden  bg-slate-100/20 backdrop-blur'></div>
				<Logo />
				{children}
			</body>
		</html>
	);
}
