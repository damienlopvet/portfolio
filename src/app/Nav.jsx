"use client";
import React, { useState } from "react";
import "./styles/nav.css";
import { FaReact, FaCss3, FaFolderOpen, FaFolder } from "react-icons/fa";
import { IoLogoJavascript } from "react-icons/io5";
import { IoMdStar, IoIosArrowDown, IoMdApps } from "react-icons/io";
import Link from "next/link";

const Nav = ({activeComponent, setActiveComponent, newComponent, setNewComponent, setPreviousComponent }) => {
	

	function handleChange(arg) {
		if (newComponent === arg) return;
		setPreviousComponent(newComponent);
		setNewComponent(arg);
	}
	
	function handleChangeTablet(arg) {
		if (newComponent != "") {
			setNewComponent("");
			setPreviousComponent("");
		}
		setActiveComponent(arg);
		toggleMenu("nav-tablet");
	}

	function toggleMenu(arg) {
		if (arg === "nav-desktop") {
			const menu = document.querySelector(".nav-desktop");
			menu.classList.toggle("menu-closed");
		}

		if (arg === "nav-tablet") {
			const menu = document.querySelector(".nav-tablet");
			menu.classList.toggle("menu-closed");
			menu.blur();
		}
	}

	return (
		<>
			<nav className='nav-desktop hidden select-none lgm:flex flex-col w-36 text-base rounded cursor-pointer shrink-0 bg-[#2D2730] text-white fixed lgm:absolute z-20 top-[100px] left-10 lgm:top-[45px]  lgm:-left-8 xl:-left-20 2xl:-left-24 shadow-lg transition-all duration-300'>
				<p className='menu-vertical-bar absolute top-[30px] left-3  bg-slate-300 m-0 p-0 h-[150px] leading-none w-[1px]'></p>
				<div className='relative item py-2 pl-1 flex flex-row gap-2 items-center cursor-default' onClick={() => toggleMenu("nav-desktop")}>
					<IoIosArrowDown className='size-4  text-[#05BCD4] arrow' />
					<FaFolderOpen className='size-4  text-[#EF5350] folder-open' />
					<FaFolder className='size-4  text-[#EF5350] folder-closed hidden' />
					<IoMdApps id='app' className='size-[11px] absolute top-[16px] left-[36px] text-[#FFCDD2]' />
					Menu
				</div>
				<Link
					href='/#AboutMe'
					className={`${newComponent === "AboutMe" && "bg-bgActiveMenu text-black active"} item py-2 flex flex-row  gap-2 items-center`}
					onClick={() => handleChange("AboutMe")}>
					<FaReact className=' menu-icon size-4 translate-x-7 text-[#05BCD4]' />
					<p className='translate-x-7'>About</p>
				</Link>
				<Link
					href='/#Works'
					scroll={false}
					className={`${newComponent === "Works" && "bg-bgActiveMenu text-black active"} item py-2 flex flex-row  gap-2 items-center`}
					onClick={() => handleChange("Works")}>
					<FaCss3 className=' menu-icon size-4 translate-x-7 text-[#42A5F5]' />
					<p className='translate-x-7'>Works</p>
				</Link>
				<Link
					href='/#Resume'
					scroll={false}
					className={`${newComponent === "Resume" && "bg-bgActiveMenu text-black active"} item py-2 flex flex-row gap-2 items-center`}
					onClick={() => handleChange("Resume")}>
					<IoLogoJavascript className=' menu-icon size-4 translate-x-7 text-[#FFCA27]' />
					<p className='translate-x-7'>Resume</p>
				</Link>
				<Link
					href='/#Contact'
					scroll={false}
					className={`${newComponent === "Contact" && "bg-bgActiveMenu text-black active"} item py-2 flex flex-row  gap-2 items-center`}
					onClick={() => handleChange("Contact")}>
					<IoMdStar className=' menu-icon size-4 translate-x-7 text-[#FFD54F]' />
					<p className='translate-x-7'>Contact</p>
				</Link>
			</nav>

			<nav className='menu-closed nav-tablet flex lgm:hidden select-none flex-col w-36 text-base rounded cursor-pointer shrink-0 bg-[#222751] text-white fixed lgm:absolute z-40 top-[20px] right-4 lgm:top-[470px] lgm:-left-8 xl:-left-20 2xl:-left-24 shadow-lg transition-all duration-300'>
				<p className='menu-vertical-bar absolute top-[30px] left-3 bg-slate-300 m-0 p-0 h-[150px] leading-none w-[1px]'></p>
				<div className='relative item py-2 pl-1 flex flex-row gap-2 items-center cursor-default' onClick={() => toggleMenu("nav-tablet")}>
					<IoIosArrowDown className='size-4  text-[#05BCD4] arrow' />
					<FaFolderOpen className='size-4  text-[#EF5350] folder-open' />
					<FaFolder className='size-4  text-[#EF5350] folder-closed hidden' />
					<IoMdApps id='app' className='size-[11px] absolute top-[16px] left-[36px] text-[#FFCDD2]' />
					Menu
				</div>
				<Link
					href='/#AboutMe'
					className={`${activeComponent === "AboutMe" && "bg-bgActiveMenu text-black active"} item py-2 flex flex-row  gap-2 items-center`}
					onClick={() => handleChangeTablet("AboutMe")}>
					<FaReact className='menu-icon size-4 translate-x-7 text-[#05BCD4]' />
					<p className='translate-x-7'>About</p>
				</Link>
				<Link
					href='/#Resume'
					className={`${activeComponent === "Resume" && "bg-bgActiveMenu text-black active"} item py-2 flex flex-row gap-2 items-center`}
					onClick={() => handleChangeTablet("Resume")}>
					<IoLogoJavascript className='menu-icon size-4 translate-x-7 text-[#FFCA27]' />
					<p className='translate-x-7'>Resume</p>
				</Link>
				<Link
					href='/#Works'
					className={`${activeComponent === "Works" && "bg-bgActiveMenu text-black active"} item py-2 flex flex-row  gap-2 items-center`}
					onClick={() => handleChangeTablet("Works")}>
					<FaCss3 className='menu-icon size-4 translate-x-7 text-[#42A5F5]' />
					<p className='translate-x-7'>Works</p>
				</Link>
				<Link
					href='/#Contact'
					className={`${activeComponent === "Contact" && "bg-bgActiveMenu text-black active"} item py-2 flex flex-row  gap-2 items-center`}
					onClick={() => handleChangeTablet("Contact")}>
					<IoMdStar className='menu-icon size-4 translate-x-7 text-[#FFD54F]' />
					<p className='translate-x-7'>Contact</p>
				</Link>
			</nav>
		</>
	);
};

export default Nav;
