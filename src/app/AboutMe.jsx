import React from "react";
import { HiCodeBracket } from "react-icons/hi2";
import { IoIosSwap } from "react-icons/io";
import { FiDatabase } from "react-icons/fi";
import { FaDocker } from "react-icons/fa";

const AboutMe = () => {
	return (
		<article id='AboutMe' className='lgm:animated-card componentSize flex flex-col bg-white overflow-y-scroll rounded-tr rounded-br'>
			{/* About me */}
			<section>
				{/* Title */}
				<div className='relative'>
					<h2 className='font-semibold text-2xl p-6'>
						<span className='text-primary'>About&nbsp;</span>Me
					</h2>
					{/* Dividers */}
					<p className='absolute bottom-0 bg-gradient-to-r from-white to-white via-slate-400 m-0 p-0 h-[1px] leading-none w-11/12'></p>
					{/* Dividers */}
				</div>
				{/* Content */}
				<div className='relative flex flex-row flex-wrap w-full'>
					<p className='hidden sm:block sm:absolute bottom-0 left-1/2 bg-gradient-to-b from-slate-400 to-white m-0 p-0 h-full leading-none w-[1px]'></p>
					<div className='sm:basis-1/2 p-6'>
						<p>
							<span className='font-semibold'>Hello! I&apos;m Damien</span> <br />I am a professional fullstack web developer, with 4 years experience building web sites
						</p>
					</div>
					<div className='sm:basis-1/2 w-full p-6 flex flex-col gap-4'>
						<div className='relative pb-4 flex flex-row items-baseline justify-between'>
							<p className='bg-primary px-2 py-[2px] text-white'>Residence</p>
							<p> Geneva</p>
							<p className='absolute bottom-0 bg-gradient-to-r from-white to-white via-slate-400 m-0 p-0 h-[1px] leading-none w-full'></p>
						</div>
						<div className='relative pb-4 flex flex-row items-baseline justify-between'>
							<p className='bg-primary px-2 py-[2px] text-white'>availability</p>
							<p> Wordwide</p>
							<p className='absolute bottom-0 bg-gradient-to-r from-white to-white via-slate-400 m-0 p-0 h-[1px] leading-none w-full'></p>
						</div>
						<div className='flex flex-row items-baseline justify-between'>
							<p className='bg-primary px-2 py-[2px] text-white'>Status</p>
							<p> Open to work</p>
						</div>
					</div>
				</div>
			</section>
			{/* Services */}
			<section>
				{/* Title */}
				<div className='relative'>
					<h2 className='font-semibold text-2xl p-6'>
						<span className='text-primary'>My&nbsp;</span>Services
					</h2>
					{/* Dividers */}
					<p className='absolute bottom-0 bg-gradient-to-r from-white to-white via-slate-400 m-0 p-0 h-[1px] leading-none w-11/12'></p>
				</div>
				{/* Content */}
				<div className='relative flex flex-row flex-wrap w-full'>
					<p className='hidden sm:block sm:absolute bottom-0 left-1/2 bg-gradient-to-b from-slate-400 to-white m-0 p-0 h-full leading-none w-[1px]'></p>
					<div className='sm:basis-1/2 p-6 flex-col flex '>
						<div className='relative pb-6 flex flex-col items-center gap-4 text-center'>
							<HiCodeBracket className='size-12 p-2 bg-primary text-white rounded-full' />
							<h3 className=''>Web Development</h3>
							<p>Highly skilled and passionate developer implementing responsive and user-friendly websites.</p>
							<p className='absolute bottom-0 bg-gradient-to-r from-white to-white via-slate-400 m-0 p-0 h-[1px] leading-none w-full'></p>
						</div>
						<div className='relative flex flex-col py-6 items-center gap-4 text-center'>
							<IoIosSwap className='size-12 p-2 bg-primary text-white rounded-full' />
							<h3 className=''>API Integration</h3>
							<p>Third Party Api Integration, Design and development of custom RESTful API&apos;s</p>
							<p className='sm:hidden absolute bottom-0 bg-gradient-to-r from-white to-white via-slate-400 m-0 p-0 h-[1px] leading-none w-full'></p>
						</div>
					</div>
					<div className='sm:basis-1/2 p-6 flex-col flex '>
						<div className='relative pb-6 flex flex-col items-center gap-4 text-center'>
							<FiDatabase className='size-12 p-[10px] bg-primary text-white rounded-full' />
							<h3 className=''>Database Maintenance</h3>
							<p>Conception and maintenance of relational and non-relational data storage solutions </p>
							<p className='absolute bottom-0 bg-gradient-to-r from-white to-white via-slate-400 m-0 p-0 h-[1px] leading-none w-full'></p>
						</div>
						<div className='flex flex-col py-6 items-center gap-4 text-center'>
							<FaDocker className='size-12 p-2 bg-primary text-white rounded-full' />
							<h3 className=''>DevOps</h3>
							<p>DevOps practices, automating workflows, and ensuring seamless collaboration for efficient software development.</p>
						</div>
					</div>
				</div>
			</section>
			{/* More about me */}
			<section>
				{/* Title */}
				<div className='relative'>
					<h2 className='font-semibold text-2xl p-6'>
						<span className='text-primary'>More&nbsp;</span>About Me
					</h2>
					{/* Dividers */}
					<p className='absolute bottom-0 bg-gradient-to-r from-white to-white via-slate-400 m-0 p-0 h-[1px] leading-none w-11/12'></p>
					{/* Dividers */}
				</div>
				{/* Content */}
				<div className='relative w-full'>
					<div className='basis-1/2 p-6'>
						<p>
							&#x2022; Build reusable code and libraries for future use. <br />
							&#x2022; Coordination, analysis, design, implementation and test of software systems <br />
							&#x2022; Design and implementation of data storage solutions <br />
							&#x2022; Design and development of server side RESTful services <br />
							&#x2022; I specialize in javascript ES, css3, php<br />
							&#x2022; Expertise in Database development sqlserver,mysql and mongoDB <br />
							&#x2022; I am able to handle cms frameworks like Wordpress and Shopify <br />
							&#x2022; Ability to understand and write documentation
						</p>
					</div>
				</div>
			</section>
		</article>
	);
};

export default AboutMe;
