import React from 'react'

const Bubbles = () => {
  return (
		<>
			<div className='bubble b1 size-10'></div>
			<div className='bubble b2 size-[30px] '></div>
			<div className='bubble b3 size-[170px] '></div>
			<div className='bubble b4 size-[100px] '></div>
			<div className='bubble b5 size-[70px] '></div>
			<div className='bubble b6 size-[40px] '></div>
			<div className='bubble b7 size-[80px] '></div>
			<div className='bubble b8 size-[30px] '></div>
			<div className='bubble b9 size-[130px] '></div>
			<div className='bubble b10 size-[150px] '></div>
			<div className='bubble animation-delay b1 size-2'></div>
			<div className='bubble animation-delay b2 size-[10px] '></div>
			<div className='bubble animation-delay b3 size-[100px] '></div>
			<div className='bubble animation-delay b4 size-[50px] '></div>
			<div className='bubble animation-delay b5 size-[30px] '></div>
			<div className='bubble animation-delay b6 size-[80px] '></div>
			<div className='bubble animation-delay b7 size-[20px] '></div>
			<div className='bubble animation-delay b8 size-[100px] '></div>
			<div className='bubble animation-delay b9 size-[70px] '></div>
			<div className='bubble animation-delay b10 size-[50px] '></div>
		</>
  );
}

export default Bubbles