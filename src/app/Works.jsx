import React from "react";
import Image from "next/image";
import Link from "next/link";
import { IoIosLink } from "react-icons/io";

const Card = ({ title, imageUrl, linkUrl }) => {
	return (
		<div className='relative flex flex-col w-fit  items-center group'>
			<Link href={linkUrl} target='_blank' className='group-hover:text-primary text-black transition-all duration-200'>
				<h3 className='font-normal mt-4'>{title}</h3>
				<div className='z-10 absolute inset-0 top-11 bottom-0 flex justify-center items-center w-full h-10/12 group-hover:bg-primary/40 transition-all duration-200 rounded'>
					<IoIosLink className='size-12 text-white rotate-45 group-hover:opacity-100 opacity-0 transition-opacity duration-300' />
				</div>
				<div className='max-w-[300px] max-h[200px] overflow-hidden rounded'>
					<Image src={imageUrl} alt={title} width={600} height={400} className='group-hover:scale-110 transition-all duration-500' />
				</div>
			</Link>
		</div>
	);
};

const Works = () => {
	return (
		<article id='Works' className='lgm:animated-card componentSize flex flex-col bg-white overflow-y-scroll rounded-tr rounded-br'>
			{/* Resume */}
			<section>
				{/* Title */}
				<div className='relative'>
					<h2 className='font-semibold text-2xl p-6'>
						<span className='text-primary'>Recent&nbsp;</span>work
					</h2>
					{/* Dividers */}
					<p className='absolute bottom-0 bg-gradient-to-r from-white to-white via-slate-400 m-0 p-0 h-[1px] leading-none w-11/12'></p>
				</div>
				{/* Content */}
				<div className='relative flex flex-row flex-wrap w-full justify-center'>
					<p className='hidden sm:block sm:absolute bottom-0 left-1/2 bg-gradient-to-b from-slate-400 to-white m-0 p-0 h-full leading-none w-[1px]'></p>
					<div className='sm:basis-1/2 p-10 pt-0 flex-col flex gap-6'>
						<Card title='Jean Noël - Medium' imageUrl={"/jean-no.jpg"} linkUrl={"https://www.jean-noel-medium.com/"} />
						<Card title='Stunify' imageUrl={"/stunify.jpg"} linkUrl={"https://stunify.io"} />
						<Card title='Ella-Maillart' imageUrl={"/ellaMaillart.jpg"} linkUrl={"https://ella-maillart.com"} />
						<Card title='Oh My food' imageUrl={"/Ohmyfood.webp"} linkUrl={"https://etwincorp.com/P3_19072021/"} />
					</div>
					<div className='sm:basis-1/2 p-10 pt-0 flex-col flex gap-6'>
						<Card title='Le Pont Du Loup' imageUrl={"/resatest.jpg"} linkUrl={"https://etwincorp.com/resatest/"} />
						<Card title='Chronomètres' imageUrl={"/multiple-chronos.webp"} linkUrl={"https://etwincorp.com/multiple-chronos/"} />
						<Card title='Temptracker' imageUrl={"/temptracker.jpeg"} linkUrl={"http://temptracker.io"} />
						<Card title='JhmediaGroup' imageUrl={"/jhmediagroup.jpg"} linkUrl={"https://jhmediagroup.com/"} />
					</div>
				</div>
			</section>
		</article>
	);
};

export default Works;
