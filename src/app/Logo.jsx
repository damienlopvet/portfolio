"use client";
import React from "react";
import "./styles/logo.css";
const Logo = () => {
	
	const triggerAnimation = () => {
		const logo = document.getElementById("logo_container");
		logo.style.animation = "none";
		setTimeout(() => {
			logo.style.animation = "scroll-watcher ease-in-out 4s";
		}, 200);
	};

	return (
		<section className='cursor-pointer fixed top-4 left-4 default-example z-30' onClick={triggerAnimation}>
			<div id='logo_container' className='transition-all'>
				<div className='face front'><span>
					&lt;!--
					</span>
					</div>
				<div className='face back'><span className="rotate-90">
					&lt;!--
					</span>
					</div>
				<div className='face right'><span>
					&lt;!--
					</span>
					</div>
				<div className='face left'><span className="rotate-45">
					&lt;!--
					</span>
					</div>
				<div className='face top'><span className="-rotate-45">
					&lt;!--
					</span>
					</div>
				<div className='face bottom'><span className="-rotate-45">
					&lt;!--
					</span>
					</div>
			</div>
		</section>	
	);
};
export default Logo;
