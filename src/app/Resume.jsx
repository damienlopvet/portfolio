import React from "react";
import { HiCodeBracket } from "react-icons/hi2";
import { IoIosSwap } from "react-icons/io";
import { FiDatabase } from "react-icons/fi";
import { FaToolbox, FaFlag, FaCheck } from "react-icons/fa";
import { FaCircle } from "react-icons/fa6";
import { GiGraduateCap } from "react-icons/gi";
import { BsFileTextFill } from "react-icons/bs";



const Resume = () => {
	return (
		<article id='Resume' className='lgm:animated-card componentSize flex flex-col bg-white overflow-y-scroll rounded-tr rounded-br'>
			{/* Resume */}
			<section>
				{/* Title */}
				<div className='relative'>
					<h2 className='font-semibold text-2xl p-6'>
						<span className='text-primary'>R</span>esume
					</h2>
					{/* Dividers */}
					<p className='hidden sm:block sm:absolute bottom-0 bg-gradient-to-r from-white to-white via-slate-400 m-0 p-0 h-[1px] leading-none w-11/12'></p>
				</div>
				{/* Content */}
				<div className='relative flex flex-row flex-wrap w-full'>
					<p className='hidden sm:block sm:absolute bottom-0 left-1/2 bg-gradient-to-b from-slate-400 to-white m-0 p-0 h-full leading-none w-[1px]'></p>
					<div className='sm:basis-1/2 p-6 flex-col flex '>
						{/* Title */}
						<div className='relative pb-6 flex flex-col gap-4'>
							<div className='flex flex-row gap-3 items-center'>
								<FaToolbox className='size-6 text-primary shrink-0' />
								<h3 className=''>EXPERIENCE</h3>
							</div>
							<p className='absolute bottom-0 bg-gradient-to-r from-white to-white via-slate-400 m-0 p-0 h-[1px] leading-none w-full'></p>
						</div>

						{/* Serenity */}
						<div className='flex flex-col py-6 gap-4'>
							<div className='text-xs text-primary border-primary border w-fit py-0.5d px-2'>2023</div>
							<div>
								<div className='text-lg leading-none'>Developer</div>
								<div className='font-light text-sm'>Halphen Serenity</div>
							</div>
							<p className='font-light'>
								&#x2022; Development of a React application <br />
								&#x2022; Data structure conception <br />
								&#x2022; configuration of Docker containers <br />
								&#x2022; Setup and management of source code management (version control with Gitlab) <br />
								&#x2022; Multi-level authentication system <br />
								&#x2022; API Integration
							</p>
						</div>

						{/* JhmediaGroup */}
						<div className='flex flex-col py-6 gap-4'>
							<div className='text-xs text-primary border-primary border w-fit py-0.5d px-2'>2022</div>
							<div>
								<div className='text-lg leading-none'>Developer</div>
								<div className='font-light text-sm'>JhMediaGroup</div>
							</div>
							<p className='font-light'>
								&#x2022; Features development <br />
								&#x2022; Debugging, refactoring <br />
								&#x2022; test and ticket reporting <br />
								&#x2022; Wordpress custom devlopment <br />
								&#x2022; SEO, Accessibility and performance optimization
							</p>
						</div>

						{/* freelance */}
						<div className='flex flex-col py-6 gap-4'>
							<div className='text-xs text-primary border-primary border w-fit py-0.5d px-2'>2019 - 2021</div>
							<div>
								<div className='text-lg leading-none'>Developer - Project Manager</div>
								<div className='font-light text-sm'>Freelance</div>
							</div>
							<p className='font-light'>
								&#x2022; Clients relations (proposal, specifications, project monitoring)
								<br />
								&#x2022; configuration of Docker containers <br />
								&#x2022; Project management and infrastructure <br />
								&#x2022; implementation Multi-level authentication system <br />
								&#x2022; Content creation and integration <br />
								&#x2022; Search Engine Optimisation
							</p>
						</div>
					</div>
					<div className='sm:basis-1/2 p-6 flex-col flex '>
						{/* Title */}
						<div className='relative pb-6 flex flex-col gap-4'>
							<div className='flex flex-row gap-3 items-center'>
								<GiGraduateCap className='size-8 text-primary shrink-0' />
								<h3 className=''>EDUCATION</h3>
							</div>
							<p className='absolute bottom-0 bg-gradient-to-r from-white to-white via-slate-400 m-0 p-0 h-[1px] leading-none w-full'></p>
						</div>
						{/* Serenity */}
						<div className='flex flex-col py-6 gap-4'>
							<div className='text-xs text-primary border-primary border w-fit py-0.5d px-2'>2021 - 2022</div>
							<div>
								<div className='text-lg leading-none'>Associate’s Degree in web development</div>
								<div className='font-light text-sm'>Openclassrooms</div>
							</div>
							<p className='font-light'>
								&#x2022; Frontend & backend development
								<br />
								&#x2022; Performance optimization
								<br />
								&#x2022; Security optimization <br />
								&#x2022; Version control
								<br />
							</p>
						</div>
					</div>
				</div>
			</section>
			{/* Skills */}
			<section>
				{/* Title */}
				<div className='relative'>
					<h2 className='font-semibold text-2xl p-6'>
						<span className='text-primary'>My&nbsp;</span>skills
					</h2>
					{/* Dividers */}
					<p className='hidden sm:block sm:absolute bottom-0 bg-gradient-to-r from-white to-white via-slate-400 m-0 p-0 h-[1px] leading-none w-full'></p>
				</div>
				{/* Content */}
				<div className='relative flex flex-row flex-wrap w-full'>
					<p className='hidden sm:block sm:absolute bottom-0 left-1/2 bg-gradient-to-b from-slate-400 to-white m-0 p-0 h-full leading-none w-[1px]'></p>
					<div className='sm:basis-1/2 p-6 flex-col flex w-full'>
						<div className='relative pb-6 flex flex-col gap-4'>
							<div className='flex flex-row gap-3 items-center justify-start'>
								<HiCodeBracket className='size-6 text-primary shrink-0' />
								<h3 className=''>CODING</h3>
							</div>
							<p className='absolute bottom-0 bg-gradient-to-r from-white to-white via-slate-400 m-0 p-0 h-[1px] leading-none w-full'></p>
						</div>

						<div className='flex flex-row justify-around gap-4'>
							<div className='relative flex flex-col py-6 gap-3'>
								<div className='size-20 z-10 rounded-full border-8 border-primary text-center flex justify-center items-center shadow-md clip-100 '></div>
								<div className='absolute size-20 border rounded-full flex justify-center items-center text-center shadow-[inset_-4px_-4px_6px_1px_#0000000d] '>100%</div>
								<div className='absolute size-20 top-6 rounded-full border-8 border-gray-400'></div>

								<p className='text-center'>Javascript</p>
							</div>
							<div className='relative flex flex-col py-6 gap-3'>
								<div className='size-20 z-10 rounded-full border-8 border-primary text-center flex justify-center items-center shadow-md clip-100 '></div>
								<div className='absolute size-20 border rounded-full flex justify-center items-center text-center shadow-[inset_-4px_-4px_6px_1px_#0000000d] '>100%</div>
								<div className='absolute size-20 top-6 rounded-full border-8 border-gray-400'></div>

								<p className='text-center'>HTML / CSS</p>
							</div>
						</div>
						<div className='flex flex-row justify-around gap-4'>
							<div className='relative flex flex-col py-6 gap-3'>
								<div className='size-20 z-10 rounded-full border-8 border-primary text-center flex justify-center items-center shadow-md clip-90 '></div>
								<div className='absolute size-20 border rounded-full flex justify-center items-center text-center shadow-[inset_-4px_-4px_6px_1px_#0000000d] '>90%</div>
								<div className='absolute size-20 top-6 rounded-full border-8 border-gray-400'></div>

								<p className='text-center'>Vue.js</p>
							</div>
							<div className='relative flex flex-col py-6 gap-3'>
								<div className='size-20 z-10 rounded-full border-8 border-primary text-center flex justify-center items-center shadow-md clip-100 '></div>
								<div className='absolute size-20 border rounded-full flex justify-center items-center text-center shadow-[inset_-4px_-4px_6px_1px_#0000000d] '>100%</div>
								<div className='absolute size-20 top-6 rounded-full border-8 border-gray-400'></div>

								<p className='text-center'>Next / React</p>
							</div>
						</div>
						<div className='flex flex-row justify-around gap-4'>
							<div className='relative flex flex-col py-6 gap-3'>
								<div className='size-20 z-10 rounded-full border-8 border-primary text-center flex justify-center items-center shadow-md clip-75 '></div>
								<div className='absolute size-20 border rounded-full flex justify-center items-center text-center shadow-[inset_-4px_-4px_6px_1px_#0000000d] '>75%</div>
								<div className='absolute size-20 top-6 rounded-full border-8 border-gray-400'></div>

								<p className='text-center'>PHP</p>
							</div>
							<div className='relative flex flex-col py-6 gap-3'>
								<div className='size-20 z-10 rounded-full border-8 border-primary text-center flex justify-center items-center shadow-md clip-80 '></div>
								<div className='absolute size-20 border rounded-full flex justify-center items-center text-center shadow-[inset_-4px_-4px_6px_1px_#0000000d] '>80%</div>
								<div className='absolute size-20 top-6 rounded-full border-8 border-gray-400'></div>

								<p className='text-center'>Docker</p>
							</div>
						</div>
					</div>
					<div className='basis-1/2 p-6 flex-col flex'>
						<div className='relative pb-6 flex flex-col gap-4'>
							<div className='flex flex-row gap-3 items-center'>
								<FaFlag className='size-6 text-primary shrink-0' />
								<h3 className=''>Languages</h3>
							</div>
							<p className='absolute bottom-0 bg-gradient-to-r from-white to-white via-slate-400 m-0 p-0 h-[1px] leading-none w-11/12'></p>
						</div>
						{/* Serenity */}
						<div className='flex flex-col py-6 gap-4'>
							<p>English</p>
							<div className='flex flex-row gap-2'>
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-slate-500' />
								<FaCircle className='size-4 text-slate-500' />
								<FaCircle className='size-4 text-slate-500' />
							</div>
						</div>
						<div className='flex flex-col py-6 gap-4'>
							<p>French</p>
							<div className='flex flex-row gap-2'>
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-primary reveal-dot' />
							</div>
						</div>
						<div className='flex flex-col py-6 gap-4'>
							<p>Spanish</p>
							<div className='flex flex-row gap-2'>
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-slate-500' />
							</div>
						</div>
						<div className='flex flex-col py-6 gap-4'>
							<p>Portuguese</p>
							<div className='flex flex-row gap-2'>
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-primary reveal-dot' />
								<FaCircle className='size-4 text-slate-500' />
								<FaCircle className='size-4 text-slate-500' />
								<FaCircle className='size-4 text-slate-500' />
								<FaCircle className='size-4 text-slate-500' />
								<FaCircle className='size-4 text-slate-500' />
								<FaCircle className='size-4 text-slate-500' />
								<FaCircle className='size-4 text-slate-500' />
							</div>
						</div>
					</div>
				</div>
			</section>
			{/* knowledge */}
			<section>
				{/* Title */}
				<div className='relative p-6 flex flex-col gap-4'>
					<div className='flex flex-row gap-3 items-center'>
						<BsFileTextFill className='size-6 text-primary shrink-0' />
						<h3 className=''>KNOWLEDGES</h3>
					</div>
					<p className='absolute bottom-0 bg-gradient-to-r from-white to-white via-slate-400 m-0 p-0 h-[1px] leading-none w-11/12'></p>
				</div>
				{/* Content */}
				<div className='relative flex flex-row max-sm:flex-wrap'>
					<p className='hidden sm:block sm:absolute bottom-0 left-1/2 bg-gradient-to-b from-slate-400 to-white m-0 p-0 h-full leading-none w-[1px]'></p>

					<div className='relative w-full space-y-4 p-6'>
						<div className='flex flex-row gap-4 items-center'>
							<FaCheck className='size-4 text-primary' />
							<p>Container orchestration</p>
						</div>
						<div className='flex flex-row gap-4 items-center'>
							<FaCheck className='size-4 text-primary' />
							<p>Serveless hosting</p>
						</div>
						<div className='flex flex-row gap-4 items-center'>
							<FaCheck className='size-4 text-primary' />
							<p>MVC frameworks</p>
						</div>
						<div className='flex flex-row gap-4 items-center'>
							<FaCheck className='size-4 text-primary' />
							<p>CMS (wordpress - shopify)</p>
						</div>
					</div>
					<div className='relative w-full space-y-4 p-6'>
						<div className='flex flex-row gap-4 items-center'>
							<FaCheck className='size-4 text-primary' />
							<p>Unit & end-to-end TEST</p>
						</div>
						<div className='flex flex-row gap-4 items-center'>
							<FaCheck className='size-4 text-primary' />
							<p>Adobe suite</p>
						</div>
						<div className='flex flex-row gap-4 items-center'>
							<FaCheck className='size-4 text-primary' />
							<p>devops automation</p>
						</div>
						<div className='flex flex-row gap-4 items-center'>
							<FaCheck className='size-4 text-primary' />
							<p>SEO</p>
						</div>
					</div>
				</div>
			</section>
		</article>
	);
};

export default Resume;
