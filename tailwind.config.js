/** @type {import('tailwindcss').Config} */
module.exports = {
	content: ["./src/pages/**/*.{js,ts,jsx,tsx,mdx}", "./src/components/**/*.{js,ts,jsx,tsx,mdx}", "./src/app/**/*.{js,ts,jsx,tsx,mdx}"],
	theme: {
		extend: {
			backgroundImage: {
				"gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
				"gradient-conic": "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
			},
			colors: {
				primary: "#1DD3B0",
				secondary: "#222751",
				ternary: "#2978A0",
				bgHoverMenu: "#64748b",
				bgHoverMenuDark: "#334155",
				bgActiveMenu: "#1DD3B0",
			},
			screens: {
				xs:"450px",
				lgm: "1120px",
			},
		},
	},
	plugins: [],
};